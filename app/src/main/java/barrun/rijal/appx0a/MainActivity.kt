package barrun.rijal.appx0a

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    lateinit var mhsAdapter : AdapterDataMhs
    var daftarMhs = mutableListOf<HashMap<String,String>>()
    val url = "http://localhost/kampus/show_data.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mhsAdapter = AdapterDataMhs(daftarMhs)
        listMhs.LayoutManager = LinearLayoutManager(this)
        listMhs.adapter = mhsAdapter
        showdataMhs()
        

        fun showDataMhs(){
            val request = StringRequest(Request.Method.POST,url,
                Response.Listener { response ->
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mhs = HashMap<String, String>()
                    mhs.put("nim", jsonObject.getString("nim"))
                    mhs.put("nama", jsonObject.getString("nama"))
                    mhs.put("nama_prodi", jsonObject.getString("nama_prodi"))
                    mhs.put("url", jsonObject.getString("url"))
                    daftarMhs.add(mhs)
                }
                mhsAdapter.notifyDataSetChanged()
                },
                Response.ErrorListener {error ->
                    Toast.makeText(this,"Terjadi Kesalahan koneksi ke server !!", Toast.LENGTH_LONG).show()
                })
            val queue = volley.newRequestQueue(this)
            queue.add(request)
        }
    }
}
